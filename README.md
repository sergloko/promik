# Rebrain_prometheus

## Отчет по выполнению итогового задания

На node1 запущены:
- [ ] [node_exporter](https://gitlab.com/sergloko/promik/-/blob/main/files/node1/node_exporter.service), как служба
- [ ] [mysql_exporter](https://gitlab.com/sergloko/promik/-/blob/main/files/node1/docker-compose.yml), в docker контейнере
- [ ] [cadvisor](https://gitlab.com/sergloko/promik/-/blob/main/files/node1/docker-compose.yml), в docker контейнере

На prometheus:
- [ ] [node_exporter](https://gitlab.com/sergloko/promik/-/blob/main/files/prometheus/node_exporter.service), как служба
- [ ] [prometheus](https://gitlab.com/sergloko/promik/-/blob/main/files/prometheus/docker-compose.yml), в docker контейнере
- [ ] [cadvisor](https://gitlab.com/sergloko/promik/-/blob/main/files/prometheus/docker-compose.yml), в docker контейнере

Конфигурация prometheus.yml:

```
global:
  scrape_interval:     15s
  evaluation_interval: 15s

rule_files:
  # - "first.rules"
  # - "second.rules"

scrape_configs:
  - job_name: prometheus
    static_configs:
      - targets: ['prometheus:9090']

  - job_name: 'node'
    static_configs:
      - targets: ['node1:9100']
      - targets: ['172.18.0.1:9100']

  - job_name: 'mysql'
    static_configs:
      - targets: ['node1:9104']

  - job_name: 'cadvisor'
    static_configs:
      - targets: ['cadvisor:8080']
      - targets: ['node1:8080']

```

Логи запуска экспортеров:

- [ ] ![cadvisor](https://gitlab.com/sergloko/promik/-/blob/main/screen/logs_cadvisor_exporter.png) 
- [ ] ![mysqld](https://gitlab.com/sergloko/promik/-/blob/main/screen/logs_start_mysql_exporter.png)
- [ ] ![node](https://gitlab.com/sergloko/promik/-/blob/main/screen/logs_start_node_exporter.png)

Cкриншоты dashboards в grafana:

- [ ] ![cadvisor(14282)](https://gitlab.com/sergloko/promik/-/blob/main/screen/screen_cadvisor_exporter.png)
- [ ] ![mysqld(7362)](https://gitlab.com/sergloko/promik/-/blob/main/screen/screen_mysql_exporter.png)
- [ ] ![node_exporter(11074)](https://gitlab.com/sergloko/promik/-/blob/main/screen/screen_node_exporter.png)

На этом у меня все.

## Благодарю за прекрасный курс!
